﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Serializer
    {
        public static byte[] Serialize<T>(T packet)
        {
            int packetID = PacketType.GetPacketID(packet);
            if (packetID == (int)PACKT_ID.Error)
                return null;

            using (var stream = new System.IO.MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(stream, packet);
                byte[] dataSource = new byte[8 + stream.Length];
                Buffer.BlockCopy(BitConverter.GetBytes((Int32)stream.Length), 0, dataSource, 0, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(packetID), 0, dataSource, 4, 4);
                Buffer.BlockCopy(stream.GetBuffer(), 0, dataSource, 8, (int)stream.Length);
                return dataSource;
            }
        }

        public static object Deserialize(int key, byte[] body)
        {
            if (body == null)
                return null;

            Type type = PacketType.GetPacketType(key);
            if (type == null)
                return null;

            using (var memStream = new System.IO.MemoryStream(body))
                return ProtoBuf.Serializer.NonGeneric.Deserialize(type, memStream);
        }
    }
}
