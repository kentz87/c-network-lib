﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class PacketType
    {
        private static Dictionary<string, int> m_packetNameDic = new Dictionary<string, int>();
        private static Dictionary<int, Type> m_packetTypeDIc = new Dictionary<int, Type>();

        public static void InitPacketType<T>()
        {
            foreach (T packetID in Enum.GetValues(typeof(T)))
            {
                if (Convert.ToInt32(packetID) == 0)
                    continue;

                string typeName = $"{ packetID.GetType().Namespace }.{ packetID.ToString()}"; 

                Type type  = typeof(T).Assembly.GetType(typeName);
                Debug.Assert(type != null);
                RegisterPacket(type, Convert.ToInt32(packetID));
            }
        }

        public static void TerminatePacketType<T>()
        {
            foreach (T packetID in Enum.GetValues(typeof(T)))
            {
                if (Convert.ToInt32(packetID) == 0)
                    continue;

                string typeName = $"{ packetID.GetType().Namespace }.{ packetID.ToString()}";

                Type type = typeof(T).Assembly.GetType(typeName);
                Debug.Assert(type != null);
                UnRegisterPacket(type, Convert.ToInt32(packetID));
            }
        }


        public static void RegisterPacket(Type type, int packetID)
        {
            m_packetNameDic.Add(type.ToString(), packetID);
            m_packetTypeDIc.Add(packetID, type);
        }

        public static void UnRegisterPacket(Type type, int packetID)
        {
            m_packetNameDic.Remove(type.ToString());
            m_packetTypeDIc.Remove(packetID);
        }

        public static int GetPacketID<T>(T packetType)
        {
            return GetPacketID(packetType.GetType());
        }

        public static int GetPacketID(Type type)
        {
            if (true == m_packetNameDic.TryGetValue(type.ToString(), out int value))
                return value;

            return (int)PACKT_ID.Error;
        }

        public static Type GetPacketType(int packetID)
        {
            if (m_packetTypeDIc.TryGetValue(packetID, out Type type))
                return type;

            return null;
        }
    }

}
