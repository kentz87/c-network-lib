﻿using System;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.Facility.Protocol;
using SuperSocket.Common;

namespace NetLibServer
{
    public class ServerRequsetInfo : BinaryRequestInfo
    {
        public int nKey { get; private set; }

        public ServerRequsetInfo(int key, byte[] body)
            : base(null, body)
        {
            this.nKey = key;
        }
    }

    public class ServerReceiveFilter : FixedHeaderReceiveFilter<ServerRequsetInfo>
    {
        public ServerReceiveFilter() 
            : base(8)
        {
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return BitConverter.ToInt32(header, offset);
        }

        protected override ServerRequsetInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            return new ServerRequsetInfo(BitConverter.ToInt32(header.Array, 4), bodyBuffer.CloneRange(offset, length));
        }
    }
}
