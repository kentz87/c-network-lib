﻿using Newtonsoft.Json;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Common;


namespace NetLibServer
{
    public abstract class Session<TAppSession> : AppSession<TAppSession, ServerRequsetInfo>
        where TAppSession : AppSession<TAppSession, ServerRequsetInfo>, IAppSession, new()
    {
        public void Send<T>(T packet)
        {
            
            byte[] serializedPacket = Serializer.Serialize(packet);
            if (serializedPacket == null)
            {
                Logger?.Warn($"Packet type is invalid Packet ID : {packet.GetType().Name }");
                return;
            }

            Send(serializedPacket, 0, serializedPacket.Length);
        }
    }

    public class ServerInstance<PACKET_TYPE, TAppSession>  : AppServer<TAppSession, ServerRequsetInfo>
        where TAppSession : Session<TAppSession>, IAppSession, new()        
    {
        private Dictionary<int, Action<TAppSession, object>> m_packetHandler = new Dictionary<int, Action<TAppSession, object>>();
        
        public ServerInstance()
            : base(new DefaultReceiveFilterFactory<ServerReceiveFilter, ServerRequsetInfo>())
        {
            PacketType.InitPacketType<PACKET_TYPE>();
            NewRequestReceived += Received;
        }

        public override void  Stop()
        {
            base.Stop();
            m_packetHandler.Clear();
            PacketType.TerminatePacketType<PACKET_TYPE>();
        }

        public void Register<T>(Action<TAppSession, object> function)
        {
            if (ServerState.NotStarted >= State)
                m_packetHandler.Add(PacketType.GetPacketID(typeof(T)), function);
        }

        public void Broadcast<T>(T packet)
        {
            byte[] serializedPacket = Serializer.Serialize(packet);
            if (serializedPacket == null)
            {
                Logger?.Warn($"Packet type is invalid Packet ID : {packet.GetType().Name }");
                return;
            }

            foreach (var session in GetAllSessions())
            {
                session.Send(serializedPacket, 0, serializedPacket.Length);
            }
        }

        private void Received(TAppSession session, ServerRequsetInfo request)
        {
            if (false == m_packetHandler.TryGetValue(request.nKey, out Action<TAppSession, object> action))
            {
                Logger?.Error($"Deserialize Failed : {request.Key}");
                return;
            }

            object obj = Serializer.Deserialize(request.nKey, request.Body);
            if (obj == null)
            {
                Logger?.Error($"Deserialize Failed : {request.Key}");
                return;
            }

            action(session, obj);
        }
    }
}
