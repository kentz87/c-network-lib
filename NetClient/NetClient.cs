﻿using System;
using System.Collections.Generic;
using SuperSocket.ClientEngine;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using Common;

namespace NetLibClient
{
    public class NetClient<PACKET_TYPE>
    {
        private EasyClient m_easyClient = new EasyClient();
        private Queue<KeyValuePair<int, object>> m_packetQueue = new Queue<KeyValuePair<int, object>>();
        private Dictionary<int, Action<object>> m_packetHandler = new Dictionary<int, Action<object>>();
        private object m_lockObj = new object();

        public event EventHandler Connected
        {
            add { m_easyClient.Connected += value; }
            remove { m_easyClient.Connected -= value; }
        }

        public event EventHandler Disconnected
        {
            add { m_easyClient.Closed += value; } 
            remove { m_easyClient.Closed -= value; }
        }

        public event EventHandler<SuperSocket.ClientEngine.ErrorEventArgs> Error
        {
            add { m_easyClient.Error += value; }
            remove { m_easyClient.Error -= value; }
        }

        public void Register<T>(Action<object> function)
        {
            if(false == m_easyClient.IsConnected)
                m_packetHandler?.Add(PacketType.GetPacketID(typeof(T)), function);
        }

        public NetClient()
        {
            PacketType.InitPacketType<PACKET_TYPE>();

            m_easyClient.Initialize(new ClientReceiveFilter(), (ClientPackage request) =>
            {
                object value = Serializer.Deserialize(request.Key, request.Body);
                if (value == null)
                {
                    return;
                }

                lock (m_lockObj)
                {
                    m_packetQueue.Enqueue(new KeyValuePair<int, object>(request.Key, value));
                }
            });
        }

        public bool Terminate()
        {
            if (null == m_easyClient || m_easyClient.IsConnected)
                return false;

            FrameMove();

            m_packetHandler.Clear();

            m_packetHandler = null;
            m_packetQueue = null;
            m_easyClient = null;

            PacketType.TerminatePacketType<PACKET_TYPE>();

            return true;
        }

        public Task<bool> Connect(string ip, int port)
        {
            Task<bool> task = m_easyClient.ConnectAsync(new IPEndPoint(IPAddress.Parse(ip), port));
            if (task.Result == false)
                m_packetHandler.Clear();

            return task;
        }

        public bool Disconnect()
        {
            if (null == m_easyClient|| false == m_easyClient.IsConnected)
                return false;

            return m_easyClient.Close().Result;
        }

        public void FrameMove()
        {
            lock (m_lockObj)
            {
                while (m_packetQueue.Count > 0)
                {
                    var packet = m_packetQueue.Dequeue();
                    if (true == m_packetHandler.TryGetValue(packet.Key, out Action<object> action))
                    {
                        action(packet.Value);
                    }
                }
            }
        }

        public void Send<T>(T t)
        {
            byte[] serializedPacket = Serializer.Serialize(t);
            if (serializedPacket == null)
                return;

            m_easyClient.Send(serializedPacket);
        }

        public string SessionID { get; set; }
    }
}