﻿using System;
using System.Linq;
using SuperSocket.ProtoBase;

namespace NetLibClient
{
    public class ClientPackage : PackageInfo<int, byte[]>
    {
        public ClientPackage(int key, byte[] body) 
            : base(key, body)
        {
        }
    }

    public class ClientReceiveFilter : FixedHeaderReceiveFilter<ClientPackage>
    {
        public ClientReceiveFilter()
            : base(8)
        {
        }

        protected override int GetBodyLengthFromHeader(IBufferStream bufferStream, int length)
        {
            return bufferStream.ReadInt32(BitConverter.IsLittleEndian);
        }

        public override ClientPackage ResolvePackage(IBufferStream bufferStream)
        {
            bufferStream.Skip(4);
            var key = bufferStream.ReadInt32(BitConverter.IsLittleEndian);
            var buffer = bufferStream.Take((int)bufferStream.Length - 8);
            return new ClientPackage(key, buffer.Last().ToArray());

        }

    }
}
